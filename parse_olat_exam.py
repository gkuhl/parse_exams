from collections import defaultdict
import csv
import os
import sys


def parse_file(filename):
    """ Parse CSV file with exam results. """
    results = []

    with open(filename, encoding='utf-8') as csv_file:

        for line in csv.reader(csv_file, delimiter='\t', quotechar='"'):

            if not line:
                continue

            if line[0] == 'Laufnummer':
                header = list(line)

            elif line[0] == 'Legende':
                break

            elif line[0].isdigit():
                results.append([l.replace('\\r\\n', '\n') for l in line])
                
    return header, [dict(v for v in zip(header, line)) for line in results]


def format_header(result, header):
    """ Format header for writing to file. """
    return '\n'.join('%s:\t%s' % (key, result[key]) for key in header[:11])
        
        
def get_questions(n, result):
    """ Get question from results by number. """
    questions = {}
    for key in result:
        if key.startswith('%d_' % n):
            questions[key] = result[key]
            
    return questions

def format_questions(result, questions):
    """ Format questions for writing to file. """
    lines = []
    
    for q in questions:
        question = get_questions(q, result)
    
        lines.append('%d. Question\n------------' % q)
        lines.append('Punkte: %s, Ende: %s, Dauer: %s s' % (
            question.get('%d_Pkt' % q),
            question.get('%d_Ende' % q),
            question.get('%d_Dauer (s)' % q)
        ))
        lines.append('')
    
        for key in question:
            if key not in ['%d_Pkt' % q, '%d_Ende' % q, '%d_Dauer (s)' % q]:
                lines.append('%s: %s' % (key, question[key]))
        
        lines.append('')
        
    return '\n'.join(lines)
        
def create_output_filename(result):
    return '%03d_%s_%s.txt' % (int(result['Laufnummer']),
                               result['Vorname'].replace(' ', '_'),
                               result['Nachname'].replace(' ', '_'))


def main(filename, output_path='.'):
    """
    Read exam results from `filename` and write text file to `output_path` for
    each student.
    """
    print('Parse:', filename)
    header, results = parse_file(filename)
    questions = sorted(set(int(h.split('_')[0]) for h in header[11:] if h))
    
    os.makedirs(output_path, exist_ok=True)
    
    for result in results:
        
        output_filename = os.path.join(output_path, create_output_filename(result))
        print('Write:', output_filename)
        
        with open(output_filename, 'w', encoding='utf-8') as outfile:       
            outfile.write(
                format_header(result, header)
            )
            outfile.write('\n\n')
            outfile.write(
                format_questions(result, questions)
            )
            
            
if __name__ == '__main__':
    filename, output_path = sys.argv[1:]
    main(filename, output_path)
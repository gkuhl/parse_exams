# README
Script for converting OLAT exam results to single text file for each student.

### Usage:
```
python parse_exam_results.py <exam_results.csv> <output_path>
```

### Contact:
Gerrit Kuhlmann (gerrit.kuhlmann@empa.ch)